# Infrastructure test page.
import os
from flask import Flask
from flask import request
from flask import Markup
from flask import render_template
from flask_sqlalchemy import SQLAlchemy


import json
import time
import redis

app = Flask(__name__)

# Configure MySQL connection.
db = SQLAlchemy()
db_uri = 'mysql://root:supersecure@db-proxy/information_schema'
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)


# Configure redis connection
redis = redis.Redis('redis') #connect to server
ttl = 31104000 #one year

def isInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

@app.route("/")
def test():
    mysql_result = False
    # TODO REMOVE FOLLOWING LINE AFTER TESTING COMPLETE.
    db.session.query("1").from_statement("SELECT 1").all()
    try:
        if db.session.query("1").from_statement("SELECT 1").all():
            mysql_result = True
    except:
        pass

    if mysql_result:
        result = Markup('<span style="color: green;">PASS</span>')
    else:
        result = Markup('<span style="color: red;">FAIL</span>')

    event = {}
    event['last_updated'] = time.time()
    event['ttl'] = ttl
    path = 'test'
    redis.delete(path) #remove old keys
    redis.hmset(path, event)
    redis.expire(path, ttl)

    if not redis.exists(path):
        return "Error: thing doesn't exist"

    event = redis.hgetall(path)
    event["ttl"] = redis.ttl(path)
    #cast integers accordingly, nested arrays, dicts not supported for now  :(
    dict_with_ints = dict((k,int(v) if isInt(v) else v) for k,v in event.iteritems())

    # Return the page with the result.
    return render_template('index.html', result=result, redis=json.dumps(dict_with_ints))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
