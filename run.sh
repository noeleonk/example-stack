#!/bin/bash

if [[ $1 = "up" ]] || [[ -z "$1" ]]; then
  # Public 'web' tier
  docker network create web
  # Internal 'app' tier'
  docker network create apps
  # Internal 'data' tier
  docker network create backend


  # flask web app on web and apps
  docker build --rm -t local/web -f web/Dockerfile web/
  docker run -d --name web1 -e SERVICE_PORTS="80" local/web
  docker network connect web web1
  docker network connect apps web1
  docker network connect backend web1

  # lb can connect only to web
  docker run -d --name lb1 --link web1:web1 --mount target=/var/run/docker.sock,source=/var/run/docker.sock,type=bind -p 8090:80 dockercloud/haproxy 
  docker network connect web lb1

  # redis is only on the apps network
  docker run -d --name redis redis:5.0.0
  docker network connect apps redis

  # db is only on the backend network
  docker volume create mysql-data
  docker run -d --name db -e MYSQL_ROOT_PASSWORD=supersecure -v mysql-data:/var/lib/mysql  mysql:5.7
  docker network connect backend db

fi

if [ "$1" == "down" ]; then
  docker rm -f web1 redis db lb1
  docker network rm web
  docker network rm apps
  docker network rm backend
fi

